from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    # <your code>
    return render_template('showpage.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    #render_template('newBook.html', books = books)
    # <your code>
    #name = "hi"
    if request.method == 'POST':
        newbook = request.form['name']
        books.append({'title': newbook, 'id':str(len(books)+1)})
        #print(newbook)
        return redirect(url_for('showBook'))
	
    else:
        return render_template('newBook.html', books = books)

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        name = request.form['name']
        tnew = {"title":str(name)}
        books[book_id-1].update(tnew)
        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', book_id = book_id)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        #book_id = book.id
        books.pop(int(book_id)-1)
        n = 1
        for i in books:
            dnew = {"id":str(n)}
            i.update(dnew)
            n+=1
        return redirect(url_for('showBook'))

    else:
        return render_template('deleteBook.html', book_id = book_id)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

